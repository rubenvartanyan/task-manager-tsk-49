package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class SessionEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();


    @Before
    @SneakyThrows
    public void before() {
    }


    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void openSessionTest() {
        final SessionDTO session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        Assert.assertNotNull(session.getUserId());
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void closeSessionTest() {
        final SessionDTO session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        endpointLocator.getSessionEndpoint().closeSession(session);
    }


}
