package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    @NotNull protected EndpointLocator endpointLocator;

    @NotNull protected Bootstrap bootstrap;

    public AbstractCommand() {
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    public void setBootstrap(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable public abstract String arg();

    @NotNull public abstract String name();

    @Nullable public abstract String description();

    public abstract void execute() throws Exception;

}
