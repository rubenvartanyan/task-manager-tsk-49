package ru.vartanyan.tm.exception.system;

public class NullObjectException extends Exception {

    public NullObjectException() throws Exception {
        super("Error! Project is not found...");
    }

}
