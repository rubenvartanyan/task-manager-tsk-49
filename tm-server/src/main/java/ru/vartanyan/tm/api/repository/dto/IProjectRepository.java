package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId);

    @Nullable
    Project findOneById(@Nullable String id);

    @Nullable
    Project findOneByIdAndUserId(
            @Nullable String userId,
            @NotNull String id
    );

    @NotNull
    Project findOneByIndex(
            @Nullable String userId,
            @NotNull Integer index
    );

    Project findOneByName(
            @Nullable String userId,
            @NotNull String name
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId,
                                @NotNull String id);

    void removeOneByName(
            @Nullable String userId,
            @NotNull String name
    );

}

