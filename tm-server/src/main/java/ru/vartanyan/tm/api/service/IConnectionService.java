package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull EntityManager getEntityManager();

}
