package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.service.model.IServiceGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.model.AbstractEntityGraph;

public abstract class AbstractServiceGraph<E extends AbstractEntityGraph> implements IServiceGraph<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractServiceGraph(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
