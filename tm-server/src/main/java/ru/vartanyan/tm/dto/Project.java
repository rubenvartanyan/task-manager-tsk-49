package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.entity.IWBS;
import ru.vartanyan.tm.listener.LoggerEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
@EntityListeners(LoggerEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@NotNull final String name,
                   @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ProjectGraph{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", dateStarted=" + dateStarted +
                ", dateFinish=" + dateFinish +
                ", created=" + created +
                '}';
    }

}
